import 'source-map-support/register'
import 'dotenv/config';
import App from "./app/App";

function bootstrap() {
  const app = new App();
  app.start();
}

bootstrap();
