import DiscordConsumer from './services/discord/DiscordConsumer';
import KafkaConsumerFactory from './services/kafka/KafkaConsumerFactory';

export default class App {
  discordConsumer
  kafkaConsumer

  constructor() {
    this.discordConsumer = DiscordConsumer;
    this.kafkaConsumer = KafkaConsumerFactory;
  }

  start() {
    // TODO
  }
}
