import { MessageMentions } from "discord.js";
import ConsumerFactory from "../../classes/ConsumerFactory";
import KafkaProducerFactory from "../kafka/KafkaProducerFactory";
import DiscordClientFactory from "./DiscordClientFactory";
// import stringify from 'json-stringify-safe';

export default new class DiscordConsumer extends ConsumerFactory {
  kafkaProducer;

  constructor() {
    super();
    this.kafkaProducer = KafkaProducerFactory;
  }

  protected create() {
    return DiscordClientFactory.get();
  }

  protected attach(): void {
    this.client.on('messageCreate', async (message) => {
      // "Ignore the message if the bot authored it"
      if (message.author.bot) return;

      // If the doesn't specifically mention the bot, return
      if (message.content.includes("@here") || message.content.includes("@everyone")) return;

      // Return if the message doesn't mention the bot
      if (!message.mentions.has(this.client.user.id)) return;

      message.content = message.content.replace(MessageMentions.USERS_PATTERN, "").trim();

      this.kafkaProducer.send(JSON.stringify(message));
    });

    if (process.env.NODE_ENV !== 'prod') {
      this.client.on('debug', async (message) => {
        console.log(message);
      });
    }
  }
}
