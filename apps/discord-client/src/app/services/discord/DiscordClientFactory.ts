import { Client, Intents } from 'discord.js'
import Environment from '../../../environments/environment';
import ClientFactory from '../../classes/ClientFactory';

export default new class DiscordClientFactory extends ClientFactory {
  protected create() {
    return new Client({ intents: [
      Intents.FLAGS.GUILDS,
      Intents.FLAGS.GUILD_MESSAGES
    ] });
  }

  protected attach() {
    this.client.on('ready', () => {
      console.log('🚀 Discord connected!');
    });
  }

  protected connect() {
    this.client.login(Environment.discord.token);
  }
}
