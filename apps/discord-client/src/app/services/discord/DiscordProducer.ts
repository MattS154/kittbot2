import ProducerFactory from "../../classes/ProducerFactory";
import DiscordClientFactory from "./DiscordClientFactory";

export default new class DiscordProducer extends ProducerFactory {
  protected create() {
    return DiscordClientFactory.get();
  }

  public send({ channelId, content }) {
    this.client.channels.cache.get(channelId).send(content)
  }

  public reply(message) {
    const { authorId, content } = message;

    this.send({
      ...message,
      content: `<@${authorId}>, ${content}`,
    })
  }
}
