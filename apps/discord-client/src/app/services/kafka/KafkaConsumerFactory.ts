import Environment from '../../../environments/environment';
import ClientFactory from '../../classes/ClientFactory';
import DiscordProducer from '../discord/DiscordProducer';
import KafkaClientFactory from './KafkaClientFactory';

export default new class KafkaConsumerFactory extends ClientFactory {
  discordProducer;

  constructor() {
    super();
    this.discordProducer = DiscordProducer;
  }
  protected create() {
    const kafka = KafkaClientFactory.get();

    return kafka.consumer({ groupId: Environment.kafka.group.id });
  }

  protected async attach() {
    const { CONNECT, DISCONNECT } = this.client.events;

    this.client.on(CONNECT, () => {
      console.log('🚀 Kafka consumer connected!');
    });

    this.client.on(DISCONNECT, () => {
      console.log('💥 Kafka consumer disconnected!');
    });

    await this.client.subscribe({ topic: 'kittbot.discord.internal.message.reply', fromBeginning: true });
    await this.client.subscribe({ topic: 'kittbot.discord.internal.message.send', fromBeginning: true });
    await this.client.run({
      eachMessage: async ({ message }) => {
        if (process.env.NODE_ENV !== 'prod'){
          console.log(message.value);
        }

        this.discordProducer.reply(JSON.parse(message.value));
      },
    });
  }

  protected connect() {
    this.client.connect();
  }
}
