import { CompressionTypes, Message } from 'kafkajs';
import ClientFactory from '../../classes/ClientFactory';
import KafkaClientFactory from './KafkaClientFactory';
import SendsMessages from '../../interfaces/SendsMessages';

export default new class KafkaProducerFactory extends ClientFactory implements SendsMessages {
  protected create() {
    const kafka = KafkaClientFactory.get();

    return kafka.producer();
  }

  protected attach() {
    const { CONNECT, DISCONNECT } = this.client.events;

    this.client.on(CONNECT, () => {
      console.log('🚀 Kafka producer connected!');
    });

    this.client.on(DISCONNECT, () => {
      console.log('💥 Kafka producer disconnected!');
    });
  }

  protected connect() {
    this.client.connect();
  }

  public send(message: string) {
    const topic = 'kittbot.discord.user.message.created';

    const messages: Array<Message> = [{
      key: 'content',
      value: message,
    }];

    const payload = {
      topic,
      messages,
      compression: CompressionTypes.GZIP,
    }

    console.log(payload);

    this.client.send(payload);
  }
}
