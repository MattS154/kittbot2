import Environment from '../../../environments/environment';
import { Kafka } from 'kafkajs';
import ClientFactory from '../../classes/ClientFactory';

export default new class KafkaConsumerFactory extends ClientFactory {
  protected create() {
    return new Kafka({
      clientId: Environment.kafka.client.id,
      brokers: Environment.kafka.hosts,
    });
  }

  // eslint-disable-next-line @typescript-eslint/no-empty-function
  protected attach() {}

  // eslint-disable-next-line @typescript-eslint/no-empty-function
  protected connect() {}
}
