import Factory from "./Factory";

export default abstract class ConsumerFactory extends Factory {
  client;

  constructor() {
    super();
    this.attach();
  }

  protected abstract attach(): void;
}
