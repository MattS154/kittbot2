export default abstract class Factory {
  client;

  constructor() {
    this.client = this.create();
  }

  protected abstract create();

  public get() {
    return this.client;
  }
}
