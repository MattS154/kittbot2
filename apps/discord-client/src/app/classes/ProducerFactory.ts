import SendsMessages from "../interfaces/SendsMessages";
import Factory from "./Factory";

export default abstract class ProducerFactory extends Factory implements SendsMessages {
  client;

  constructor() {
    super();
  }

  public abstract send(message): void;
}
