import Factory from "./Factory";

export default abstract class ClientFactory extends Factory {
  client;

  constructor() {
    super();
    this.attach();
    this.connect();
  }

  protected abstract attach(): void;

  protected abstract connect(): void;
}
