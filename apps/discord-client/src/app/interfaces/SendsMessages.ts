export default interface SendsMessages {
  send(message): void;
}
