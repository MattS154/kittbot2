const Environment = {
  production: false,
  discord: {
    token: process.env.DISCORD_TOKEN
  },
  kafka: {
    client: {
      name: process.env.KAFKA_CLIENT_NAME,
      id: process.env.KAFKA_CLIENT_ID,
    },
    group: {
      id: process.env.KAFKA_GROUP_ID,
    },
    hosts: [process.env.KAFKA_HOST]
  }
};

export default Environment;
