import { CompressionTypes, Kafka, Message } from "kafkajs";
import axios from 'axios';
import errorMessages from "./config/errorMessages";
import { get } from "lodash";

const baseURL = process.env.REDDIT_HOST;

axios.defaults.baseURL = baseURL;

const inTopic = 'kittbot.discord.internal.command.bunny.run';
const outTopic = 'kittbot.discord.internal.message.send';

const client = new Kafka({
  clientId: process.env.KAFKA_CLIENT_ID,
  brokers: [process.env.KAFKA_HOST],
});

const producer = client.producer();

{
  const { CONNECT, DISCONNECT } = producer.events;

  producer.on(CONNECT, () => {
    console.log('🚀 Kafka producer connected!');
  });

  producer.on(DISCONNECT, () => {
    console.log('💥 Kafka producer disconnected!');
  });
}

const consumer = client.consumer({ groupId: process.env.KAFKA_GROUP_ID });

{
  const { CONNECT, DISCONNECT } = consumer.events;

  consumer.on(CONNECT, () => {
    console.log('🚀 Kafka consumer connected!');
  });

  consumer.on(DISCONNECT, () => {
    console.log('💥 Kafka consumer disconnected!');
  });
}

const run = async () => {
  await producer.connect();
  await consumer.connect();
  await consumer.subscribe({ topic: inTopic, fromBeginning: true });
  await consumer.run({
    eachMessage: async ({ message }) => {
      const discordMessage = JSON.parse(message.value.toString());
      let redditResponse;
      let axiosError;
      let response;

      function send(message) {
        const messages: Array<Message> = [{
          value: JSON.stringify({
            ...discordMessage,
            content: message,
          }),
        }];

        const payload = {
          topic: outTopic,
          messages,
          compression: CompressionTypes.GZIP,
        };

        producer.send(payload);
      }

      try {
        do {
          redditResponse = await axios.get('/r/rabbits/random.json', {
            timeout: 5000,
            params: {
              cache: 0,
            },
          });
        } while (get(redditResponse, 'data[0].data.children[0].data.is_video'))
      } catch (error) {
        axiosError = error;
      }

      if (axiosError) {
        send(errorMessages[axiosError.code] ?? `an unknown error occurred (${axiosError.code})`);
        return;
      }

      const redditPost = get(redditResponse, 'data[0].data.children[0].data');
      const postUrl: string = get(redditPost, 'url');

      if (postUrl.indexOf('gallery') > 0) {
        const galleryUrlJson = postUrl.replace('https://www.reddit.com/gallery', '/comments') + '.json';

        const galleryResponse = await axios.get(galleryUrlJson, {
          timeout: 5000,
        });

        const firstId = get(galleryResponse, 'data[0].data.children[0].data.gallery_data.items[0].media_id');
        const firstImageData = get(galleryResponse, `data[0].data.children[0].data.media_metadata.${firstId}.s.u`);
        const firstImage = new URL(firstImageData);

        firstImage.hostname = 'i.redd.it';
        firstImage.search = '';

        response = firstImage.href;
      } else {
        response = postUrl;
      }

      send(response);
    },
  });
}

run().catch(e => console.error(`[example/consumer] ${e.message}`, e));
