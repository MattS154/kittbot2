export default {
  INVALID_PARAMETERS: 'Invalid number of parameters. Usage: number [min] [max]',
  INVALID_MIN: "The 'min' is invalid. Enter a number.",
  INVALID_MAX: "The 'max' is invalid. Enter a number.",
  MIN_PARAM_EXCEEDS_UNSAFE_MIN: 'The "min" parameter is exceeds the lowest safe integer value. Please use a higher value.',
  MIN_PARAM_EXCEEDS_UNSAFE_MAX: 'The "min" parameter is exceeds the highest safe integer value. Please use a lower value.',
  MAX_PARAM_EXCEEDS_UNSAFE_MIN: 'The "max" parameter is exceeds the lowest safe integer value. Please use a higher value.',
  MAX_PARAM_EXCEEDS_UNSAFE_MAX: 'The "max" parameter is exceeds the highest safe integer value. Please use a lower value.',
}
