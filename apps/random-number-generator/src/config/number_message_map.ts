/*
 * Jackpot numbers are 69 and 777
 * “Straight” - number that goes in order (Ex: 234, 678, 456, 567). Payout is 25k gil.
 * “Royal Straight” - 123 or 789. Payout is 50k gil.
 * “Three of a Kind” - a number roll that has three of same numbers. (Example: 111, 555, 999).  Payout is 25k gil.
 */

const CONGRATULATIONS = 'Congratulations!'
const BETTER_LUCK_NEXT_TIME = 'Better luck next time.'
const TWENTY_FIVE_THOUSAND_GIL = '25k gil will be delivered via mogmail.'
const FIFTY_THOUSAND_GIL = '50k gil will be delivered via mogmail.'
const STRAIGHT = 'A straight!';
const THREE_OF_A_KIND = 'A three of a kind!';
const ROYAL_STRAIGHT = 'A royal straight!';
const JACKPOT = 'You hit the jackpot!';

const SAM_MENTION = '<@323202290750193665>';
const ADA_MENTION = '<@507726834033754112>';

export default {
  0: `. The same amount of gil you win for rolling this number. ${BETTER_LUCK_NEXT_TIME}`,
  1: `. You know what they say: first is the worst. ${BETTER_LUCK_NEXT_TIME}`,
  9: `. ${ADA_MENTION}'s favorite number! ${BETTER_LUCK_NEXT_TIME}`,
  42: `. You found the meaning of life, the universe, and everything. Just not gil. ${BETTER_LUCK_NEXT_TIME}`,
  69: `! ${JACKPOT} Nice. Tell them what they win ${SAM_MENTION}!`,
  111: `! ${THREE_OF_A_KIND} ${CONGRATULATIONS} ${TWENTY_FIVE_THOUSAND_GIL}`,
  123: `! ${ROYAL_STRAIGHT} ${CONGRATULATIONS} ${FIFTY_THOUSAND_GIL}`,
  222: `! ${THREE_OF_A_KIND} ${CONGRATULATIONS} ${TWENTY_FIVE_THOUSAND_GIL}`,
  234: `! ${STRAIGHT} ${CONGRATULATIONS} ${TWENTY_FIVE_THOUSAND_GIL}`,
  333: `! ${THREE_OF_A_KIND} ${CONGRATULATIONS} ${TWENTY_FIVE_THOUSAND_GIL}`,
  345: `! ${STRAIGHT} ${CONGRATULATIONS} ${TWENTY_FIVE_THOUSAND_GIL}`,
  404: `. Number not found. ${BETTER_LUCK_NEXT_TIME}`,
  418: `. I'm a teapot. ${BETTER_LUCK_NEXT_TIME}`,
  420: `. Blaze it. ${BETTER_LUCK_NEXT_TIME}`,
  444: `! ${THREE_OF_A_KIND} ${CONGRATULATIONS} ${TWENTY_FIVE_THOUSAND_GIL}`,
  456: `! ${STRAIGHT} ${CONGRATULATIONS} ${TWENTY_FIVE_THOUSAND_GIL}`,
  555: `! ${THREE_OF_A_KIND} ${CONGRATULATIONS} ${TWENTY_FIVE_THOUSAND_GIL}`,
  567: `! ${STRAIGHT} ${CONGRATULATIONS} ${TWENTY_FIVE_THOUSAND_GIL}`,
  666: `! ${THREE_OF_A_KIND} Congratulations? Just keep your distance, Satan. ${TWENTY_FIVE_THOUSAND_GIL}`,
  678: `! ${STRAIGHT} ${CONGRATULATIONS} ${TWENTY_FIVE_THOUSAND_GIL}`,
  777: `! ${JACKPOT} Tell them what they win ${SAM_MENTION}!`,
  789: `! ${ROYAL_STRAIGHT} ${CONGRATULATIONS} ${FIFTY_THOUSAND_GIL}`,
  888: `! ${THREE_OF_A_KIND} ${CONGRATULATIONS} ${TWENTY_FIVE_THOUSAND_GIL}`,
  999: `! ${THREE_OF_A_KIND} ${CONGRATULATIONS} ${TWENTY_FIVE_THOUSAND_GIL}`,
  1000: `. Impressive but not worth anything. ${BETTER_LUCK_NEXT_TIME}`,
  default: `. ${BETTER_LUCK_NEXT_TIME}`,
};
