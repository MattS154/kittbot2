import { CompressionTypes, Kafka, Message } from "kafkajs";
import axios from 'axios';
import { get as _get } from 'lodash';
import ERROR from './config/error_messages';
import MESSAGES from './config/number_message_map';

const baseURL = process.env.CATAAS_HOST;

axios.defaults.baseURL = baseURL;

const inTopic = 'kittbot.discord.internal.command.rng.run';
const outTopic = 'kittbot.discord.internal.message.reply';

const client = new Kafka({
  clientId: process.env.KAFKA_CLIENT_ID,
  brokers: [process.env.KAFKA_HOST],
});

const producer = client.producer();

{
  const { CONNECT, DISCONNECT } = producer.events;

  producer.on(CONNECT, () => {
    console.log('🚀 Kafka producer connected!');
  });

  producer.on(DISCONNECT, () => {
    console.log('💥 Kafka producer disconnected!');
  });
}

const consumer = client.consumer({ groupId: process.env.KAFKA_GROUP_ID });

{
  const { CONNECT, DISCONNECT } = consumer.events;

  consumer.on(CONNECT, () => {
    console.log('🚀 Kafka consumer connected!');
  });

  consumer.on(DISCONNECT, () => {
    console.log('💥 Kafka consumer disconnected!');
  });
}

const run = async () => {
  await producer.connect();
  await consumer.connect();
  await consumer.subscribe({ topic: inTopic, fromBeginning: true });
  await consumer.run({
    eachMessage: async ({ message }) => {
      function send(response: string) {
        const messages: Array<Message> = [{
          value: JSON.stringify({
            ...discordMessage,
            content: response,
          }),
        }];

        const payload = {
          topic: outTopic,
          messages,
          compression: CompressionTypes.GZIP,
        };

        producer.send(payload);
      }

      const discordMessage = JSON.parse(message.value.toString());

      const params = discordMessage.content.split(' ');

      if (params.length != 3) {
        send(ERROR.INVALID_PARAMETERS);
        return;
      }

      const min = Math.ceil(parseInt(params[1], 10));
      const max = Math.floor(parseInt(params[2], 10));

      if (!Number.isFinite(min)) {
        send(ERROR.INVALID_MIN);
        return;
      }

      if (!Number.isFinite(min)) {
        send(ERROR.INVALID_MAX);
        return;
      }

      if (min <= Number.MIN_SAFE_INTEGER) {
        send(ERROR.MIN_PARAM_EXCEEDS_UNSAFE_MIN);
        return;
      }

      if (min >= Number.MAX_SAFE_INTEGER) {
        send(ERROR.MIN_PARAM_EXCEEDS_UNSAFE_MAX);
        return;
      }

      if (max <= Number.MIN_SAFE_INTEGER) {
        send(ERROR.MAX_PARAM_EXCEEDS_UNSAFE_MIN);
        return;
      }

      if (max >= Number.MAX_SAFE_INTEGER) {
        send(ERROR.MAX_PARAM_EXCEEDS_UNSAFE_MAX);
        return;
      }

      const randomNumber = Math.floor(Math.random() * (max - min) + min);

      const specialMessage = String(_get(MESSAGES, randomNumber, MESSAGES.default));

      const response = `you rolled a **${randomNumber}**${specialMessage}`;

      send(response);
    },
  });
}

run().catch(e => console.error(`[example/consumer] ${e.message}`, e));
