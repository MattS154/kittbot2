import { CompressionTypes, Kafka, Message } from "kafkajs";

const inTopic = 'kittbot.discord.user.message.created';
const outTopic = 'kittbot.discord.internal.message.send';

const client = new Kafka({
  clientId: 'kittbot.test.dummy',
  brokers: ['kafka:9092'],
});

const producer = client.producer();

{
  const { CONNECT, DISCONNECT } = producer.events;

  producer.on(CONNECT, () => {
    console.log('🚀 Kafka producer connected!');
  });

  producer.on(DISCONNECT, () => {
    console.log('💥 Kafka producer disconnected!');
  });
}

const consumer = client.consumer({ groupId: 'kittbot.test-group' });

{
  const { CONNECT, DISCONNECT } = consumer.events;

  consumer.on(CONNECT, () => {
    console.log('🚀 Kafka consumer connected!');
  });

  consumer.on(DISCONNECT, () => {
    console.log('💥 Kafka consumer disconnected!');
  });
}

const run = async () => {
  await producer.connect();
  await consumer.connect();
  await consumer.subscribe({ topic: inTopic, fromBeginning: true });
  await consumer.run({
    eachMessage: async ({ topic, partition, message }) => {
      const prefix = `${topic}[${partition} | ${message.offset}] / ${message.timestamp}`;

      console.log(`- ${prefix} ${message.key}#${message.value}`);

      const messages: Array<Message> = [message];

      const payload = {
        topic: outTopic,
        messages,
        compression: CompressionTypes.GZIP,
      };

      producer.send(payload);
    },
  });
}

run().catch(e => console.error(`[example/consumer] ${e.message}`, e));
