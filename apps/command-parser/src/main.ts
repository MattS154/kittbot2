import { CompressionTypes, Kafka, Message } from "kafkajs";

const inTopic = 'kittbot.discord.user.message.created';

const client = new Kafka({
  clientId: process.env.KAFKA_CLIENT_ID,
  brokers: [process.env.KAFKA_HOST],
});

const producer = client.producer();

{
  const { CONNECT, DISCONNECT } = producer.events;

  producer.on(CONNECT, () => {
    console.log('🚀 Kafka producer connected!');
  });

  producer.on(DISCONNECT, () => {
    console.log('💥 Kafka producer disconnected!');
  });
}

const consumer = client.consumer({ groupId: process.env.KAFKA_GROUP_ID });

{
  const { CONNECT, DISCONNECT } = consumer.events;

  consumer.on(CONNECT, () => {
    console.log('🚀 Kafka consumer connected!');
  });

  consumer.on(DISCONNECT, () => {
    console.log('💥 Kafka consumer disconnected!');
  });
}

const run = async () => {
  await producer.connect();
  await consumer.connect();
  await consumer.subscribe({ topic: inTopic, fromBeginning: true });
  await consumer.run({
    eachMessage: async ({ message }) => {
      let outTopic: string;
      const discordMessage = JSON.parse(message.value.toString());

      if (process.env.NODE_ENV !== 'prod') {
        console.log(discordMessage);
      }

      if (discordMessage.content == 'cat') {
        outTopic = 'kittbot.discord.internal.command.cat.run';
      } else if (
        discordMessage.content == 'bunny' ||
        discordMessage.content == 'rabbit' ||
        discordMessage.content == 'bunnies'
      ) {
          outTopic = 'kittbot.discord.internal.command.bunny.run';
      } else if (discordMessage.content.split(' ')[0] === 'number') {
        outTopic = 'kittbot.discord.internal.command.rng.run';
      } else {
        return;
      }

      const messages: Array<Message> = [message];

      const payload = {
        topic: outTopic,
        messages,
        compression: CompressionTypes.GZIP,
      };

      producer.send(payload);
    },
  });
}

run().catch(e => console.error(`[example/consumer] ${e.message}`, e));
